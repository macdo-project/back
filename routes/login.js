const express = require('express'),
    app = express.Router(),
    sha512 = require('js-sha512'),
    jwt = require('jsonwebtoken'),
    expressJwt = require('express-jwt');

const { User } = require('../database');

app.post('/login', (req, res) => {

    if (req.body.email && req.body.password) {
        User.findOne({
            where: {
                email: req.body.email,
                password: sha512.sha512(req.body.password)
            }
        }).then((user) => {
            let token = jwt.sign({ id: user.id, type: 'User' }, process.env.JWT_SECRET);

            res.status(200).json({
                status: 200,
                data: token
            })
        })
    }else{
        res.status(400).json({
            status: 400
        })
    }

})

app.get('/protected', expressJwt({ secret: process.env.JWT_SECRET, algorithms: ['HS256'] }), (req, res) => {

    User.findOne({
        where: {
            id: req.user.id
        }
    }).then((user) => {

        res.status(200).json({
            status: 200,
            data: {
                secret: "Kerry James m'a invité à son concert",
                user: user
            }
        })

    })

})

// app.all('/protected*', expressJwt({ secret: process.env.JWT_SECRET, algorithms: ['HS256'] }), (req, res, next) => {

//     User.findOne({
//         where: {
//             id: req.user.id
//         }
//     }).then((user) => {
//         req.user = user
//         next()

//     })
// })



app.post('/users', (req, res) => {

    User.create({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: sha512.sha512(req.body.password),
        role: 3
    }).then((user) => {
        res.status(201).json({
            status: 201,
            data: user
        })
    })

})

module.exports = app
