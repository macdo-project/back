const express = require('express'),
    app = express.Router()

const { ProductCategory } = require('../database');

app.get('/categories', (req, res) => {
    ProductCategory.findAll().then((categories) => {
        return res.status(200).json({
            status: 200,
            data: categories
        })
    })
})

module.exports = app
