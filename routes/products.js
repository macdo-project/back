const express = require('express'),
    app = express.Router()

const { Product } = require('../database');

app.get('/product', (req, res) => {
    Product.findAll().then((products) => {
        return res.status(200).json({
            status: 200,
            data: products
        })
    })
})

app.get('/product/category/:id', (req, res) => {
    Product.findAll({
            where: {product_category_id: req.params.id}
        }).then((products) => {
        return res.status(200).json({
            status: 200,
            data: products
        })
    })
})

module.exports = app
