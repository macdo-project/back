const express = require('express'),
    app = express.Router(),
    jwt = require('jsonwebtoken')

const { Terminal } = require('../database');

app.post('/auth', (req, res) => {
    if (req.body.idBorne){
        Terminal.findOne({
            where: {
                uid: req.body.idBorne
            }
        }).then((terminal) => {
            if (terminal && terminal.active){
                let token = jwt.sign({ id: terminal.id, type: 'terminal', kitchen: terminal.kitchen }, process.env.JWT_SECRET);

                return res.status(200).json({
                    status: 200,
                    data: token
                })
            }
            res.status(400).json({
                status: 400,
                message: "Borne non-active"
            })
        })
    }
})

module.exports = app
