require('dotenv').config()

const express = require('express')
const app = express();
const server = require('http').createServer(app);
const cors = require("cors");
const methodOverride = require('method-override')
const { sequelize, Task } = require('./database')

//const tasksRoute = require('./routes/tasks')
//const ordersRoute = require('./routes/orders')
const authRoute = require('./routes/auth')
const loginRoute = require('./routes/login')
const productRoute = require('./routes/products')
const categoriesRoute = require('./routes/productCategories')


app.use(express.json())
app.use(cors())
app.use(methodOverride())

//app.use(tasksRoute)
//app.use(ordersRoute)

app.use(authRoute)
app.use(loginRoute)
app.use(productRoute)
app.use(categoriesRoute)

server.listen(process.env.NODE_HTTP_PORT);
