const { Model, DataTypes } = require("sequelize");

class ProductCategory extends Model {}

module.exports = (sequelize) => {

    ProductCategory.init({

        name: {
            type: DataTypes.STRING,
            allowNull: false
        },

        image: {
            type: DataTypes.STRING,
            allowNull: false
        },

    }, {
        sequelize: sequelize,
        underscored: true
    });

    return ProductCategory

}
