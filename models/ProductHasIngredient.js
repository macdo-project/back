const { Model, DataTypes } = require("sequelize");

class ProductHasIngredient extends Model {}

module.exports = (sequelize) => {

    ProductHasIngredient.init({

        product_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        ingredient_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        quantity: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1
        },

    }, {
        sequelize: sequelize,
        underscored: true
    });

    return ProductHasIngredient

}
