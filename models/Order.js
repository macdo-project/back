const { Model, DataTypes } = require("sequelize");

class Order extends Model {}

module.exports = (sequelize) => {

    Order.init({

        terminal_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        created_at: {
            type: DataTypes.DATE,
            allowNull: false
        },

        status: {
            type: DataTypes.STRING,
            allowNull: false
        }

    }, {
        sequelize: sequelize,
        underscored: true,
    });

    return Order

}
