const { Model, DataTypes } = require("sequelize");

class Terminal extends Model {}

module.exports = (sequelize) => {

    Terminal.init({

        uid: {
            type: DataTypes.STRING,
            allowNull: false
        },

        kitchen: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },

        active: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },

    }, {
        sequelize: sequelize,
        underscored: true
    });

    return Terminal

}
