const { Model, DataTypes } = require("sequelize");

class ProductHasProduct extends Model {}

module.exports = (sequelize) => {

    ProductHasProduct.init({

        main_product_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        product_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

    }, {
        sequelize: sequelize,
        underscored: true
    });

    return ProductHasProduct

}
