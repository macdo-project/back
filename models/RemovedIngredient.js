const { Model, DataTypes } = require("sequelize");

class RemovedIngredient extends Model {}

module.exports = (sequelize) => {

    RemovedIngredient.init({

        client_order_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        ingredient_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

    }, {
        sequelize: sequelize,
        underscored: true
    });

    return RemovedIngredient

}
