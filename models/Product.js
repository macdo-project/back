const { Model, DataTypes } = require("sequelize");

class Product extends Model {}

module.exports = (sequelize) => {

    Product.init({

        short: {
            type: DataTypes.STRING,
            allowNull: false
        },

        name: {
            type: DataTypes.STRING,
            allowNull: false
        },

        price: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        picture: {
            type: DataTypes.STRING,
            allowNull: false
        },

    }, {
        sequelize: sequelize,
        underscored: true
    });

    return Product

}
