const { Model, DataTypes } = require("sequelize");

class OrderProduct extends Model {}

module.exports = (sequelize) => {

    OrderProduct.init({

        order_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },

        product_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },

        quantity: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1
        },

    }, {
        sequelize: sequelize,
        tableName: 'orders_has_products',
        underscored: true
    });

    return OrderProduct

}
