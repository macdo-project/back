const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('macdo', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    underscored: true,
    define: {
        underscored: true,
    }
});

let Order = require('./models/Order')(sequelize)
let OrderProduct = require('./models/OrderProduct')(sequelize)
let Product = require('./models/Product')(sequelize)
let ProductCategory = require('./models/ProductCategory')(sequelize)
let Terminal = require('./models/Terminal')(sequelize)
let User = require('./models/User')(sequelize)

ProductCategory.hasMany(Product)
Product.belongsTo(ProductCategory)

Order.belongsToMany(Product, { through: OrderProduct })
Product.belongsToMany(Order, { through: OrderProduct })


sequelize.sync({
    alter: true
})

module.exports = {
    sequelize: sequelize,
    Order: Order,
    Product: Product,
    ProductCategory: ProductCategory,
    Terminal: Terminal,
    User: User
}
